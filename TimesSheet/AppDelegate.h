//
//  AppDelegate.h
//  TimesSheet
//
//  Created by 渡辺 克裕 on 13/02/03.
//  Copyright (c) 2013年 渡辺 克裕. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface AppDelegate : UIResponder <UIApplicationDelegate>

@property (strong, nonatomic) UIWindow *window;

@end
