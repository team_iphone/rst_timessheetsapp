//
//  DetailViewController.m
//  TimesSheet
//
//  Created by 渡辺 克裕 on 13/02/03.
//  Copyright (c) 2013年 渡辺 克裕. All rights reserved.
//

#import "DetailViewController.h"

@interface DetailViewController ()
- (void)configureView;
@end

@implementation DetailViewController
@synthesize cellValue;

#pragma mark - Managing the detail item

/*
- (void)setDetailItem:(id)newDetailItem
{
    if (_detailItem != newDetailItem) {
        _detailItem = newDetailItem;
        
        // Update the view.
        [self configureView];
    }
}
*/
- (void)configureView
{
    // Update the user interface for the detail item.

    if (self.detailItem) {
        self.detailDescriptionLabel.text = [self.detailItem description];
    }
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    // Do any additional setup after loading the view, typically from a nib.
    [self configureView];


    self.label1.text = cellValue;
    if (cellValue==@"01/01") {
        self.label2.text = @"01/01（火） 9:00-17:00";
    }
    else if (cellValue==@"01/02") {
        self.label2.text = @"01/02（水） 9:30-17:30";
    }
    else if (cellValue==@"01/03") {
        self.label2.text = @"01/03（木） 10:00-18:00";
    }
    else if (cellValue==@"01/04") {
        self.label2.text = @"01/04（金） 10:30-18:30";
    }
    else if (cellValue==@"01/05") {
        self.label2.text = @"01/05（土） 00:00-00:00";
    }
    else if (cellValue==@"01/06") {
        self.label2.text = @"01/06（日） 00:00-00:00";
    }
    else if (cellValue==@"01/07") {
        self.label2.text = @"01/07（月） 09:00-17:00";
    }
    else if (cellValue==@"01/08") {
        self.label2.text = @"01/08（火） 09:30-17:30";
    }
    else if (cellValue==@"01/09") {
        self.label2.text = @"01/09（水） 10:00-18:00";
    }
    else if (cellValue==@"01/10") {
        self.label2.text = @"01/10（木） 10:30-18:30";
    }
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

@end
