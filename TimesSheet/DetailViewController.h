//
//  DetailViewController.h
//  TimesSheet
//
//  Created by 渡辺 克裕 on 13/02/03.
//  Copyright (c) 2013年 渡辺 克裕. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface DetailViewController : UIViewController
{
    NSString *cellValue;
}
@property (nonatomic,retain) NSString *cellValue;

@property (weak, nonatomic) IBOutlet UILabel *label1;
@property (weak, nonatomic) IBOutlet UILabel *label2;

@property (strong, nonatomic) id detailItem;

@property (weak, nonatomic) IBOutlet UILabel *detailDescriptionLabel;
@end
